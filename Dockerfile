FROM ubuntu
RUN apt-get update -y &&\
    apt-get install -y python3-pip python3-dev


COPY ./requirments.txt /app/requirments.txt


WORKDIR /app


RUN pip install -r requirments.txt

COPY . /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]